package com.example.poko.newsinshortservice;

import android.support.v4.app.*;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends FragmentActivity {
    private VerticalViewPager mPager;

    private PagerAdapter mPagerAdapter;
    public List<SingleStory> mSingleStories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager = (VerticalViewPager) findViewById(R.id.pager);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://startupbriefs.today/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        APIshortNews newsShort_api = retrofit.create(APIshortNews.class);
        Call<StoriesResponse> call = newsShort_api.fetchStories(1);

        call.enqueue(new Callback<StoriesResponse>() {
            @Override
            public void onResponse(Call<StoriesResponse> call, Response<StoriesResponse> response) {
                mSingleStories=response.body().getstory();
                mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mSingleStories);
                mPager.setAdapter(mPagerAdapter);

            }

            @Override
            public void onFailure(Call<StoriesResponse> call, Throwable t) {
                Log.d("Codekamo", t.getMessage());
                t.printStackTrace();
            }
        });


    }

   /* @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {

            super.onBackPressed();
        } else {

            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }*/
}
