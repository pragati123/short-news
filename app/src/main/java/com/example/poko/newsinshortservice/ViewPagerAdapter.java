package com.example.poko.newsinshortservice;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Poko on 07-07-2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public List<SingleStory> list;
    public ViewPagerAdapter(FragmentManager fm, List<SingleStory> list) {
        super(fm);
        this.list=list;
    }

    @Override
    public Fragment getItem(int position) {
        FragmentActivity fragmentActivity=FragmentActivity.newInstance(0,0);
        fragmentActivity.title=this.list.get(position).getTitle();
        fragmentActivity.description=this.list.get(position).getDescription();
        fragmentActivity.time=this.list.get(position).getCreatedAt();
        fragmentActivity.imageurl=this.list.get(position).getThumbnailUrl();

        return fragmentActivity;
    }



    @Override
    public int getCount() {
        return list.size();
    }
}
