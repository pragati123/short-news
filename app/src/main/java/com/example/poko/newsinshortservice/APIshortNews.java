package com.example.poko.newsinshortservice;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Poko on 07-07-2016.
 */
public interface APIshortNews {

    @GET("stories")
    Call<StoriesResponse> fetchStories(@Query("page") int pageNo);
}
