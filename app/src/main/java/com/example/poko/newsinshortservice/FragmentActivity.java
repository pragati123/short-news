package com.example.poko.newsinshortservice;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;



public class FragmentActivity extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private int mParam1;
    private int mParam2;
    public TextView titleTextView;
    public TextView descriptionTextView;
    public TextView createdatTextView;
    public ImageView url;
    public String title = " ";
    public String time = " ";
    public String description = " ";
    public String imageurl = " ";
    public Context mContext;

    public FragmentActivity(){

    }

    public static FragmentActivity newInstance(int param1, int param2) {
        FragmentActivity fragment = new FragmentActivity();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=this.getContext();
       // JodaTimeAndroid.init(mContext);

            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.activity_fragment, container, false);

        titleTextView = (TextView) rootView.findViewById(R.id.title_textView);
        createdatTextView = (TextView) rootView.findViewById(R.id.createdat_textview);
        descriptionTextView = (TextView) rootView.findViewById(R.id.description_textview);
        url = (ImageView) rootView.findViewById(R.id.url_imageview);
        this.titleTextView.setText(this.title);
        this.createdatTextView.setText(this.time);
        this.descriptionTextView.setText(this.description);
        // this.url.setI(this.imageurl);

       // Picasso.with(this.getActivity()).load(imageurl).into(url);
        Picasso.with(this.getActivity()).load(imageurl).into(url);


        return rootView;
    }
}